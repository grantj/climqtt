/* 
 * Copyright (c) 2016, Grant Jones <grant@gxjones.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of climqtt nor the names of its contributors may be
 *     used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

#include <ctype.h>	// isalnum for debugging

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
 #include <fcntl.h>

#include "climqtt.h"




int CLIMQTT_sock_loop( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t **incoming_pkt ) {
    struct timeval tv;
	fd_set readfds;
	
	*incoming_pkt = NULL;

    tv.tv_sec = 2;
    tv.tv_usec = 500000;

    FD_ZERO(&readfds);
    FD_SET(cli->sockfd, &readfds);
    if( select(cli->sockfd+1, &readfds, NULL, NULL, &tv) == -1 ) {
		perror("select");
		return -1;
    }

	if (FD_ISSET(cli->sockfd, &readfds)) {
		cli->current_pkt = CLIMQTT_read_packet( cli, cli->current_pkt );
		if( !cli->current_pkt ) {
			printf("CLIMQTT_sock_loop read packet returned no packet!?\n");
			exit(1);
			return 0;
		}

		if( cli->current_pkt->read_state == CLIMQTT_PRS_Complete ) {
			CLIMQTT_cli_handle_packet_resp( cli, cli->current_pkt );

			*incoming_pkt = cli->current_pkt;

			cli->current_pkt = NULL;
		} else if( cli->current_pkt->read_state == CLIMQTT_PRS_Error ) {
			printf("Err happened while receiving a MQTT packet.\n");
			cli->current_pkt = NULL;
		}

	} else {
		cli->last_ping_time = climqtt_time();
		CLIMQTT_Packet_t *pingreq_pkt = CLIMQTT_mkpkt_pingreq();
		CLIMQTT_write_packet( cli, pingreq_pkt );
		CLIMQTT_free_pkt(pingreq_pkt);
		return 0;
	}
	
	//	TODO: if we're never hitting the above pingreq sending code, we'll have to force one if our keepalive time is up.
	if( climqtt_time() - cli->last_ping_time  > 2.5 ) {
		cli->last_ping_time = climqtt_time();
		CLIMQTT_Packet_t *pingreq_pkt = CLIMQTT_mkpkt_pingreq();
		CLIMQTT_write_packet( cli, pingreq_pkt );
		CLIMQTT_free_pkt(pingreq_pkt);
		printf("forced ping.\n");
	}

	return 0;
}


void CLIMQTT_sock_disconnect( CLIMQTT_Client_t *cli ) {
	if(!cli) {
		return;
	}

	close(cli->sockfd); 
}

int CLIMQTT_sock_connect(CLIMQTT_Client_t *cli, const char *host, const char *port ) {

	struct addrinfo hints;
	struct addrinfo *servinfo = NULL;
	struct addrinfo *p;
	int status;
	int sockfd;
	if(!cli) {
		return -1;
	}
	cli->last_ping_time = climqtt_time();

	cli->host = strdup( host );
	cli->port = strdup( port );

	memset(&hints, 0, sizeof hints); // make sure the struct is empty
	hints.ai_family = AF_UNSPEC;     // don't care IPv4 or IPv6
	hints.ai_socktype = SOCK_STREAM; // TCP stream sockets
	hints.ai_flags = AI_PASSIVE;     // fill in my IP for me

	if ((status = getaddrinfo(host, port, &hints, &servinfo)) != 0) {
	    fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
	    return -1;
	}

	for(p = servinfo;p != NULL; p = p->ai_next) {
        void *addr;

        // get the pointer to the address itself,
        // different fields in IPv4 and IPv6:
        if (p->ai_family == AF_INET) { // IPv4
            struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
            addr = &(ipv4->sin_addr);
            cli->ipver = "IPv4";
        } else { // IPv6
            struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
            addr = &(ipv6->sin6_addr);
            cli->ipver = "IPv6";
        }

    	if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            continue;
        }

        // convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, cli->ipstr, sizeof(cli->ipstr) );

		fcntl(sockfd, F_SETFL, O_NONBLOCK);

        break;
    }

    freeaddrinfo(servinfo); // free the linked list

	printf("%s: %s\n", cli->ipver, cli->ipstr);
	cli->sockfd = sockfd;

	return 0;
}
