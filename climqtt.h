#ifndef CLIMQTT_H
#define CLIMQTT_H

/*
docs: http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html
beej's network: http://beej.us/guide/bgnet/output/html/multipage/clientserver.html\

http://www.eclipse.org/paho/files/mqttdoc/Cclient/

Create a client object
Set the options to connect to an MQTT server
Set up callback functions if multi-threaded (asynchronous mode) operation is being used (see Asynchronous vs synchronous client applications).
Subscribe to any topics the client needs to receive
Repeat until finished:
	Publish any messages the client needs to
	Handle any incoming messages
Disconnect the client
Free any memory being used by the client

*/

typedef enum CLIMQTT_Packet_Read_State {
	CLIMQTT_PRS_Fresh = 0,
	CLIMQTT_PRS_DecodingRemainingLength,
	CLIMQTT_PRS_ReadingPayload,
	CLIMQTT_PRS_Complete,
	CLIMQTT_PRS_Error
} CLIMQTT_Packet_Read_State_t;


typedef enum CLIMQTT_Packet_Type {
	CLIMQTT_PACKET_TYPE_RESERVED0 = 0,
	CLIMQTT_PACKET_TYPE_CONNECT,
	CLIMQTT_PACKET_TYPE_CONNACK,
	CLIMQTT_PACKET_TYPE_PUBLISH,
	CLIMQTT_PACKET_TYPE_PUBACK,
	CLIMQTT_PACKET_TYPE_PUBREC,
	CLIMQTT_PACKET_TYPE_PUBREL,
	CLIMQTT_PACKET_TYPE_PUBCOMP,
	CLIMQTT_PACKET_TYPE_SUBSCRIBE,
	CLIMQTT_PACKET_TYPE_SUBACK,
	CLIMQTT_PACKET_TYPE_UNSUBSCRIBE,
	CLIMQTT_PACKET_TYPE_UNSUBACK,
	CLIMQTT_PACKET_TYPE_PINGREQ,
	CLIMQTT_PACKET_TYPE_PINGRESP,
	CLIMQTT_PACKET_TYPE_DISCONNECT,
	CLIMQTT_PACKET_TYPE_RESERVED15
} CLIMQTT_Packet_Type_t;


static uint8_t CLIMQTT_Packet_Default_Flags[16] = {
	0x0, // MQTT_PACKET_TYPE_RESERVED0 = 0,
	0x0, // MQTT_PACKET_TYPE_CONNECT,
	0x0, // MQTT_PACKET_TYPE_CONNACK,
	0x0, // MQTT_PACKET_TYPE_PUBLISH,
	0x0, // MQTT_PACKET_TYPE_PUBACK,
	0x0, // MQTT_PACKET_TYPE_PUBREC,
	0x2, // MQTT_PACKET_TYPE_PUBREL,
	0x0, // MQTT_PACKET_TYPE_PUBCOMP,
	0x2, // MQTT_PACKET_TYPE_SUBSCRIBE,
	0x0, // MQTT_PACKET_TYPE_SUBACK,
	0x2, // MQTT_PACKET_TYPE_UNSUBSCRIBE,
	0x0, // MQTT_PACKET_TYPE_UNSUBACK,
	0x0, // MQTT_PACKET_TYPE_PINGREQ,
	0x0, // MQTT_PACKET_TYPE_PINGRESP,
	0x0, // MQTT_PACKET_TYPE_DISCONNECT,
	0x0, // MQTT_PACKET_TYPE_RESERVED15
};

#define CLIMQTT_PACKET_FLAG_PUBLISH_DUP 	(1 << 3)
#define CLIMQTT_PACKET_FLAG_PUBLISH_QOS_H	(1 << 2)
#define CLIMQTT_PACKET_FLAG_PUBLISH_QOS_L	(1 << 1)
#define CLIMQTT_PACKET_FLAG_RETAIN			(1 << 0)

#define CLIMQTT_MAX_RM_BUF_SZ		10	// 6 arbitrarily chosen 10 bytes



typedef struct {
	CLIMQTT_Packet_Read_State_t read_state;

	CLIMQTT_Packet_Type_t type;
	uint8_t flags;

	uint64_t RemainingLength;
	

	uint64_t write_cursor;
	uint64_t rd_sz;
	uint8_t *rd;

	// Remaining Length Encoding (streaming):
	uint8_t rm_buf[CLIMQTT_MAX_RM_BUF_SZ];
	uint32_t rm_buf_len;

} CLIMQTT_Packet_t;


typedef struct CLIMQTT_Client {
	char *host;
	char *port;

	char *ipver;
	char ipstr[INET6_ADDRSTRLEN];

	int sockfd;

	uint16_t next_packet_id;
	CLIMQTT_Packet_t *current_pkt;

	time_t last_ping_time;


	int (*receive_packet)__P((const struct CLIMQTT_Client *, const CLIMQTT_Packet_t * ));
} CLIMQTT_Client_t;

CLIMQTT_Client_t *CLIMQTT();
time_t climqtt_time(void);

int CLIMQTT_sock_connect(CLIMQTT_Client_t *cli, const char *host, const char *port );
int CLIMQTT_sock_loop( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t **incoming_pkt );
void CLIMQTT_sock_disconnect( CLIMQTT_Client_t *cli );

void CLIMQTT_cli_handle_packet_resp( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *pkt );

void CLIMQTT_write_packet( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *pkt );
CLIMQTT_Packet_t * CLIMQTT_read_packet( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *current_pkt );

void CLIMQTT_describe_packet( CLIMQTT_Packet_t *pkt );
void CLIMQTT_free_pkt( CLIMQTT_Packet_t *pkt );


CLIMQTT_Packet_t *CLIMQTT_mkpkt_connect(uint8_t flags, uint16_t keepalive, const char *client_id );
CLIMQTT_Packet_t *CLIMQTT_mkpkt_publish( int dup, int QoS, int retain, const char *topic, const uint8_t *buf, uint32_t buflen );
CLIMQTT_Packet_t *CLIMQTT_mkpkt_subscribe( const char *topic, uint16_t packet_id, int QoS );
CLIMQTT_Packet_t *CLIMQTT_mkpkt_pingreq( );
CLIMQTT_Packet_t *CLIMQTT_mkpkt_disconnect( );

void CLIMQTT_get_publish_payload( CLIMQTT_Packet_t *pkt, uint32_t *len, uint8_t **buf );
void CLIMQTT_get_publish_topic( CLIMQTT_Packet_t *pkt, uint16_t *len, uint8_t **buf );

#endif