/* 
 * Copyright (c) 2016, Grant Jones <grant@gxjones.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of climqtt nor the names of its contributors may be
 *     used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

#include <ctype.h>	// isalnum for debugging

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
 #include <fcntl.h>

#include <sys/time.h>		// climqtt_time

#include "climqtt.h"


const char *CLIMQTT_packet_type_str[] = {
	"CLIMQTT_PACKET_TYPE_RESERVED0",
	"CLIMQTT_PACKET_TYPE_CONNECT",
	"CLIMQTT_PACKET_TYPE_CONNACK",
	"CLIMQTT_PACKET_TYPE_PUBLISH",
	"CLIMQTT_PACKET_TYPE_PUBACK",
	"CLIMQTT_PACKET_TYPE_PUBREC",
	"CLIMQTT_PACKET_TYPE_PUBREL",
	"CLIMQTT_PACKET_TYPE_PUBCOMP",
	"CLIMQTT_PACKET_TYPE_SUBSCRIBE",
	"CLIMQTT_PACKET_TYPE_SUBACK",
	"CLIMQTT_PACKET_TYPE_UNSUBSCRIBE",
	"CLIMQTT_PACKET_TYPE_UNSUBACK",
	"CLIMQTT_PACKET_TYPE_PINGREQ",
	"CLIMQTT_PACKET_TYPE_PINGRESP",
	"CLIMQTT_PACKET_TYPE_DISCONNECT",
	"CLIMQTT_PACKET_TYPE_RESERVED15",
};


static void CLIMQTT__pkt_write( CLIMQTT_Packet_t *pkt, const uint8_t *bytes, uint32_t len ) {
	uint8_t *nb = realloc( pkt->rd, pkt->rd_sz + len );
	if( !nb ) {
		exit(1);
	}
	pkt->rd = nb;
	memcpy( &nb[pkt->rd_sz], bytes, len );
	pkt->rd_sz += len;	
}

static void CLIMQTT__pkt_write_cstr( CLIMQTT_Packet_t *pkt, const char *cstr ) {
	uint16_t len = strlen( cstr );
	uint8_t len_buf[2+4] = { len>>8, len & 0xFF };
	CLIMQTT__pkt_write(pkt, len_buf, 2 );
	CLIMQTT__pkt_write(pkt, (const uint8_t *)cstr, len );
}


static int CLIMQTT_encode_length( uint64_t x, uint8_t *out_buf ) {
	uint8_t *ptr = out_buf;
	do { 
		int encodedByte = x % 128;
		x = x / 128;
		
		// if there are more data to encode, set the top bit of this byte
		if ( x > 0 ) {
			encodedByte = encodedByte | 0x80;
		}

		*ptr++ = encodedByte;
	} while ( x > 0 );

	return (ptr - out_buf);
}

// returns number of bytes read:
static int CLIMQTT_decode_length( uint8_t *length_bytes, uint32_t length_bytes_len, uint64_t *value_out ) {
	int multiplier = 1;
	uint64_t value = 0;
	uint8_t *ptr = length_bytes;
	uint64_t encodedByte = 0;
	do {
		// needs a byte:
		if( (ptr - length_bytes) >= length_bytes_len ) {
			return 0;	// need more bytes!
		}
	    encodedByte = *ptr++;
	    value += (encodedByte & 127) * multiplier;
	    multiplier *= 128;
	    if (multiplier > 128*128*128*128) {
	    	printf("malformed remaining length\n");
	    	return -1;
	    }
	} while ((encodedByte & 128) != 0);
	*value_out = value;
	return (ptr - length_bytes);
}



CLIMQTT_Packet_t *CLIMQTT_mkpkt( CLIMQTT_Packet_Type_t type ) {
	CLIMQTT_Packet_t * pkt = calloc(1, sizeof(CLIMQTT_Packet_t) );
	if( type > CLIMQTT_PACKET_TYPE_RESERVED15 ) {
		printf("Invalid packet type: %d\n", type);
		return NULL;
	}
	pkt->type = type;
	pkt->flags = CLIMQTT_Packet_Default_Flags[pkt->type];

	return pkt;
}

void CLIMQTT_free_pkt( CLIMQTT_Packet_t *pkt ) {
	if(!pkt) {
		return;
	}
	if( pkt->rd ) {
		free(pkt->rd);
		pkt->rd = NULL;
	}

	free( pkt );
}

void CLIMQTT_write_packet( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *pkt ) {

	// write byte 1: control type  & 4-bit reserve
	uint8_t buf = (pkt->type << 4) | pkt->flags;
	// write byte 2+: length
	uint8_t encoded_len_buf[5] = {0};
	int enc_len_buf_sz = CLIMQTT_encode_length( pkt->rd_sz, encoded_len_buf );

	send( cli->sockfd, &buf, 1, 0);
	send( cli->sockfd, encoded_len_buf, enc_len_buf_sz, 0);
	send( cli->sockfd, pkt->rd, pkt->rd_sz, 0 );



}


static int CLIMQTT_net_recv( CLIMQTT_Client_t *cli, void *buf, int recv_len, int flags ) {
	int len = recv( cli->sockfd, buf, recv_len, flags);
	//printf("CLIMQTT_net_recv: wanted %d bytes; got %d bytes\n", recv_len, len );

	return len;
}

static void CLIMQTT_set_pkt_read_state( CLIMQTT_Packet_t *current_pkt, CLIMQTT_Packet_Read_State_t state ) {
	current_pkt->read_state = state;
	//printf("current_pkt->read_state = %d;\n", );
}

CLIMQTT_Packet_t * CLIMQTT_read_packet( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *current_pkt ) {
	int len;
	uint8_t buf = 0x00;

	// byte 1: control & flags byte:
	if( !current_pkt ) {
		len = CLIMQTT_net_recv( cli, &buf, 1, 0);

		if( len != 1 ) {
			printf("len != 1 (len = %d)\n",len);
			return NULL;
		}
	
		current_pkt = CLIMQTT_mkpkt( (buf & 0xF0) >> 4 );
		if(!current_pkt) {
			printf("!current_pkt\n");
			return NULL;
		}
		
		current_pkt->flags = buf & 0x0F;

		CLIMQTT_set_pkt_read_state(current_pkt, CLIMQTT_PRS_Fresh);
	}


	// byte 2+: encoded remaining length:
	if( current_pkt->read_state == CLIMQTT_PRS_Fresh || 
		current_pkt->read_state == CLIMQTT_PRS_DecodingRemainingLength ) {

		do {
			uint64_t value_out = 0;
			int decoder_result = CLIMQTT_decode_length( current_pkt->rm_buf, current_pkt->rm_buf_len, &value_out );
			if( decoder_result > 0 ) {
				current_pkt->RemainingLength = value_out;
				CLIMQTT_set_pkt_read_state(current_pkt, CLIMQTT_PRS_ReadingPayload);
				break;
			} else if( decoder_result != 0 ) {
				// another decoder error occured.
				printf("decode length err\n");
				CLIMQTT_set_pkt_read_state(current_pkt, CLIMQTT_PRS_Error);
				return current_pkt;
			}

			// try read:
			if( current_pkt->rm_buf_len >= CLIMQTT_MAX_RM_BUF_SZ ) {
				printf("rm_buf_len length err\n");
				CLIMQTT_set_pkt_read_state(current_pkt, CLIMQTT_PRS_Error);
				return current_pkt;
			}
			len = CLIMQTT_net_recv( cli, &current_pkt->rm_buf[current_pkt->rm_buf_len], 1, 0);
			if( len == 1 ) {
				current_pkt->rm_buf_len += 1;
			}
		} while( len == 1 );

	}

	if( current_pkt->read_state == CLIMQTT_PRS_ReadingPayload ) {

		// read reamining data:
		while( current_pkt->rd_sz < current_pkt->RemainingLength ) {
			len = CLIMQTT_net_recv(cli, &buf, 1, 0);
			if(len != 1 ) {
				return current_pkt;
			}
			CLIMQTT__pkt_write(current_pkt, &buf, 1 );
		}
		CLIMQTT_set_pkt_read_state( current_pkt, CLIMQTT_PRS_Complete );
	}

	
	return current_pkt;
}


static int isprintable( uint8_t chr ) {
	if( isalnum(chr) || ispunct(chr) || isspace(chr) ) {
		return 1;
	}

	return 0;
}
void CLIMQTT_describe_packet( CLIMQTT_Packet_t *pkt ) {
	if(!pkt) {
		return;
	}
	printf("Packet: %s\n", CLIMQTT_packet_type_str[pkt->type]);

	if( pkt->type == CLIMQTT_PACKET_TYPE_PUBLISH ) {
		uint32_t payload_len;
		uint8_t *payload_buf;
		uint16_t topic_len;
		uint8_t *topic_buf;

		CLIMQTT_get_publish_payload( pkt, &payload_len, &payload_buf );
		CLIMQTT_get_publish_topic( pkt, &topic_len, &topic_buf );

		printf("Topic: %.*s\n", topic_len, topic_buf );
		int print_len = payload_len;
		if( print_len > 512 ) {
			print_len = 512;
		}
		int i;
		printf("\t");
		for( i = 0; i < print_len; i ++) {
			printf("%c", isprintable(payload_buf[i]) ? payload_buf[i] : '.' );
		}
		printf("\n");

	
	}
#if 0
	printf("\tremaining length: %d\n", (int)pkt->RemainingLength);

	int i;
	for( i = 0; i < pkt->RemainingLength; ) {
		printf("%.7x ", i);
		int o;
		for( o = 0; o < 16 && o+i < pkt->RemainingLength; o ++ ) {
			printf("%.2x", pkt->rd[i +o] );
			if( (o+1) % 2 == 0 ) {
				printf(" ");
			}
		}
		for( o = 0; o < 16 && o+i < pkt->RemainingLength; o ++ ) {
			printf("%c", isalnum(pkt->rd[i+o]) ? pkt->rd[i+o] : '.' );
		}
		printf("\n");

		i += 16;
	}
#endif
}

void CLIMQTT_get_publish_topic( CLIMQTT_Packet_t *pkt, uint16_t *len, uint8_t **buf ) {
	uint8_t *ptr = pkt->rd;
	uint16_t topic_len = (ptr[0] << 8) | ptr[1]; ptr += 2;
	*len = topic_len;
	*buf = ptr;
}
void CLIMQTT_get_publish_payload( CLIMQTT_Packet_t *pkt, uint32_t *len, uint8_t **buf ) {
	uint8_t *ptr = pkt->rd;
	uint16_t topic_len = (ptr[0] << 8) | ptr[1]; ptr += 2;
	ptr += topic_len;
	*len = pkt->RemainingLength - topic_len - 2;
	*buf = ptr;
}
static uint16_t CLIMQTT_get_publish_packetID( CLIMQTT_Packet_t *pkt ) {
	uint8_t *ptr = pkt->rd;
	uint16_t topic_len = (ptr[0] << 8) | ptr[1]; ptr += 2;
	ptr += topic_len;

	return (ptr[0] << 8) | ptr[1];
}
static uint16_t CLIMQTT_get_pubrel_packetID( CLIMQTT_Packet_t *pkt ) {
	uint8_t *ptr = pkt->rd;
	return (ptr[0] << 8) | ptr[1];	
}


CLIMQTT_Packet_t *CLIMQTT_mkpkt_connect(uint8_t flags, uint16_t keepalive, const char *client_id ) {

	CLIMQTT_Packet_t *pkt = CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_CONNECT );

	uint8_t hdr[6] = {0x00, 0x04, 'M', 'Q', 'T', 'T' };
	CLIMQTT__pkt_write(pkt, hdr, 6);
	uint8_t level[1] = {0x04};
	CLIMQTT__pkt_write(pkt, level, 1);
	CLIMQTT__pkt_write(pkt, &flags, 1 );
	uint8_t ka_bytes[2] = { (keepalive >> 8), keepalive & 0xFF };
	CLIMQTT__pkt_write(pkt, ka_bytes, 2 );

	// write payload (client ID):
	CLIMQTT__pkt_write_cstr(pkt, client_id );

	return pkt;
}

CLIMQTT_Packet_t *CLIMQTT_mkpkt_publish( int dup, int QoS, int retain, const char *topic, const uint8_t *buf, uint32_t buflen ) {
	CLIMQTT_Packet_t *pkt = CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_PUBLISH );

	if( dup ) {
		pkt->flags |= CLIMQTT_PACKET_FLAG_PUBLISH_DUP;
	}
	pkt->flags |= (QoS & 0x3) << 1;
	if( (pkt->flags & 0x6) == 0x6) {
		printf("QoS is invalid\n");
		exit(1);
	} 
	pkt->flags |= retain ? CLIMQTT_PACKET_FLAG_RETAIN : 0;

	CLIMQTT__pkt_write_cstr(pkt, topic );
	if( QoS > 0 ) {
		// need to write a packet ID
		printf("needs to wriet packet ID\n");
	}

	// write app specific:
	CLIMQTT__pkt_write(pkt, buf, buflen);

	return pkt;
}


CLIMQTT_Packet_t *CLIMQTT_mkpkt_subscribe( const char *topic, uint16_t packet_id, int QoS ) {
	CLIMQTT_Packet_t *pkt = CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_SUBSCRIBE );

	uint8_t packet_id_bytes[2] = { (packet_id >> 8), packet_id & 0xFF };
	CLIMQTT__pkt_write(pkt, packet_id_bytes, 2 );

	CLIMQTT__pkt_write_cstr(pkt, topic );
	// write QoS
	uint8_t QoS_buf[1] = { QoS & 0x3 };
	CLIMQTT__pkt_write(pkt, QoS_buf, 1);

	return pkt;
}

CLIMQTT_Packet_t *CLIMQTT_mkpkt_pingreq( ) {
	return CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_PINGREQ );
}

CLIMQTT_Packet_t *CLIMQTT_mkpkt_disconnect( ) {
	CLIMQTT_Packet_t *pkt = CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_DISCONNECT );

	return pkt;
}

time_t climqtt_time(void)
{
	struct timeval tv;
	gettimeofday(&tv, NULL); 
	return tv.tv_sec;
}

CLIMQTT_Client_t *CLIMQTT() {
	CLIMQTT_Client_t *cli = calloc( 1, sizeof(CLIMQTT_Client_t));	
	cli->next_packet_id = 1;
	return cli;
}

void CLIMQTT_cli_handle_packet_resp( CLIMQTT_Client_t *cli, CLIMQTT_Packet_t *pkt ) {
	if( pkt->type == CLIMQTT_PACKET_TYPE_PUBLISH && (pkt->flags & CLIMQTT_PACKET_FLAG_PUBLISH_QOS_H || pkt->flags & CLIMQTT_PACKET_FLAG_PUBLISH_QOS_L) ) {
		// QoS 2 or QoS 1
		
		uint16_t packet_id = CLIMQTT_get_publish_packetID( pkt );

		CLIMQTT_Packet_t *pkt_resp = CLIMQTT_mkpkt( (pkt->flags & CLIMQTT_PACKET_FLAG_PUBLISH_QOS_L) ? CLIMQTT_PACKET_TYPE_PUBACK : CLIMQTT_PACKET_TYPE_PUBREC );
		uint8_t packet_id_bytes[2] = { (packet_id >> 8), packet_id & 0xFF };
		CLIMQTT__pkt_write(pkt_resp, packet_id_bytes, 2 );
		CLIMQTT_write_packet( cli, pkt_resp );
		CLIMQTT_free_pkt(pkt_resp);
	}

	if( pkt->type == CLIMQTT_PACKET_TYPE_PUBREL ) {
		uint16_t packet_id = CLIMQTT_get_pubrel_packetID( pkt );

		CLIMQTT_Packet_t *pkt_resp = CLIMQTT_mkpkt( CLIMQTT_PACKET_TYPE_PUBCOMP );
		uint8_t packet_id_bytes[2] = { (packet_id >> 8), packet_id & 0xFF };
		CLIMQTT__pkt_write(pkt_resp, packet_id_bytes, 2 );
		CLIMQTT_write_packet( cli, pkt_resp );
		CLIMQTT_free_pkt(pkt_resp);
	}
}
