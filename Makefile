CC=cc
CFLAGS=-I. -Werror -ggdb
DEPS = climqtt.h
OBJ = climqtt.o climqtt_sock.o main.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

climqtt: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f *.o