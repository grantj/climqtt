/* 
 * Copyright (c) 2016, Grant Jones <grant@gxjones.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of climqtt nor the names of its contributors may be
 *     used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "climqtt.h"

#include <ctype.h>	// isalnum for debugging

#include <time.h>

int main( int argc, char *argv[] ) {
	srand48(time(NULL));
	uint32_t r = lrand48();
	uint32_t r2 = lrand48();
	char client_id[256];
	snprintf(client_id, 256, "%.8x/%.8x", r, r2);
	printf("client_id: %s\n", client_id);

	CLIMQTT_Client_t *cli = CLIMQTT();

	//CLIMQTT_sock_connect(cli, "127.0.0.1", "1883");
	CLIMQTT_sock_connect(cli, "iot.eclipse.org", "1883");
	

	CLIMQTT_Packet_t * pkt = CLIMQTT_mkpkt_connect( 0x02, 10, client_id );
	CLIMQTT_write_packet( cli, pkt );

	CLIMQTT_Packet_t *incoming_pkt = NULL;
	while( !CLIMQTT_sock_loop( cli, &incoming_pkt ) ) {
		if( !incoming_pkt ) {
			continue;
		}

		if( incoming_pkt->type == CLIMQTT_PACKET_TYPE_PINGRESP ) {
			// yes, wonderful the server is still there...
			continue;
		}

		CLIMQTT_describe_packet( incoming_pkt );
			
		if( incoming_pkt->type == CLIMQTT_PACKET_TYPE_CONNACK ) {
			//const char *channel = "/test";
			const char *channel = "#";
			int QoS = 0;
			CLIMQTT_Packet_t * pkt3 = CLIMQTT_mkpkt_subscribe( channel, cli->next_packet_id++, QoS );
			CLIMQTT_write_packet( cli, pkt3 );

		}

		CLIMQTT_free_pkt( incoming_pkt );

	}
#if 0
	read_and_describe_pkt(cli);

	char *test_pub_str = "hello world!";
	CLIMQTT_Packet_t * pkt2 = CLIMQTT_mkpkt_publish( 0, 0, 0, "test", (uint8_t *)test_pub_str, strlen(test_pub_str) );
	CLIMQTT_write_packet( cli, pkt2 );
	//CLIMQTT_read_packet(sockfd);	// QoS >= 1

	
	read_and_describe_pkt(cli);

	while(1) {
		read_and_describe_pkt(cli);		
	}


	CLIMQTT_Packet_t * pkt_disc = CLIMQTT_mkpkt_disconnect();
	CLIMQTT_write_packet( cli, pkt_disc);
	read_and_describe_pkt(cli);
#endif

 	CLIMQTT_sock_disconnect(cli);


	return 0;

}

